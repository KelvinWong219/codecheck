/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djf.ui;

import java.io.File;

/**
 *
 * @author KelvinWongNYC
 */
public class SortFile implements Comparable{
    
    private long blackboardTime;
    private long codeTime;
    private long projectsTime;
    private long submissionsTime;
    public long time;
    public File currentFile;
    
    public SortFile(File file){
        currentFile = file;
        File blackboard = new File(file.getAbsoluteFile() + "/blackboard");
        if(blackboard.exists()){
            blackboardTime = blackboard.lastModified();
        }      
        File code = new File(file.getAbsoluteFile() + "/code");
        if(code.exists()){
            codeTime = code.lastModified();
        }
        File projects = new File(file.getAbsoluteFile() + "/projects");
        if(projects.exists()){
            projectsTime = projects.lastModified();
        }
        File submissions = new File(file.getAbsoluteFile()+"/submissions");{
            submissionsTime = submissions.lastModified();
        }
        
        if(blackboardTime >= codeTime && blackboardTime >= projectsTime && blackboardTime >= submissionsTime){
        time = blackboardTime;
        }else if(codeTime >= blackboardTime && codeTime>=projectsTime && codeTime >= submissionsTime){
        time = codeTime;
        }else if(projectsTime >= blackboardTime && projectsTime >= codeTime && projectsTime >= submissionsTime){
        time = projectsTime;
        }else{
        time = submissionsTime;
        }
 
    }

    @Override
    public int compareTo(Object o) {
        
        long current = ((SortFile)o).time;
        if(current < time){
        return -1;
        } else if (current > time){
        return 1;
        }else {
        return 0;
        }
    }
    
}
