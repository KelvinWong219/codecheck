/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.data;

import sc.CodeCheckApp;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import java.io.File;
import javafx.collections.FXCollections;

/**
 *
 * @author KelvinWongNYC
 */
public class CodeCheckData implements AppDataComponent {

    CodeCheckApp app;
    ObservableList<File> step1;
    ObservableList<File> step2;
    ObservableList<File> step3;
    ObservableList<File> step4;
    ObservableList<File> step5;

    File currentFile;

    public CodeCheckData(CodeCheckApp app) {
        this.app = app;
        step1 = FXCollections.observableArrayList();
        step2 = FXCollections.observableArrayList();
        step3 = FXCollections.observableArrayList();
        step4 = FXCollections.observableArrayList();
        step5 = FXCollections.observableArrayList();

    }

    public void setCurrentFile() {
        currentFile = app.getGUI().getFileController().getCurrentFile();
    }

    public void loadStep1Data() {

        File blackboard = new File("work/" + currentFile.getName() + "/blackboard/");
        if (blackboard.exists()) {
            File[] sub = blackboard.listFiles();
            for (File file : sub) {
                String fileExtension = ".zip";
                if (file.getName().toLowerCase().contains(fileExtension)) {
                    step1.add(file);
                }
            }
        }
    }

    public void loadStep2Data() {
        File blackboard = new File("work/" + currentFile.getName() + "/submissions/");
        if (blackboard.exists()) {
            File[] sub = blackboard.listFiles();
            for (File file : sub) {
                String fileExtension = ".zip";
                if (file.getName().toLowerCase().contains(fileExtension)) {
                    step2.add(file);
                }
            }
        }
    }

    public void loadStep3Data() {
        File blackboard = new File("work/" + currentFile.getName() + "/submissions/");
        if (blackboard.exists()) {
            File[] sub = blackboard.listFiles();
            for (File file : sub) {
                String fileExtension = ".zip";
                if (file.getName().toLowerCase().contains(fileExtension)) {
                    step3.add(file);
                }
            }
        }
    }

    public void loadStep4Data() {
        File blackboard = new File("work/" + currentFile.getName() + "/projects/");
        if (blackboard.exists()) {
            File[] sub = blackboard.listFiles();
            for (File file : sub) {
                if (file.isDirectory()) {
                    step4.add(file);
                }
            }
        }

    }

    public void loadStep5Data() {
        File blackboard = new File("work/" + currentFile.getName() + "/code/");
        if (blackboard.exists()) {
            File[] sub = blackboard.listFiles();
            for (File file : sub) {
                if (file.isDirectory()) {
                    step5.add(file);
                }
            }
        }

    }

    public ObservableList<File> getStep1Data() {
        return step1;
    }

    public ObservableList<File> getStep2Data() {
        return step2;
    }

    public ObservableList<File> getStep3Data() {
        return step3;
    }

    public ObservableList<File> getStep4Data() {
        return step4;
    }

    public ObservableList<File> getStep5Data() {
        return step5;
    }

    public String getCurrentFileName() {
        return currentFile.getName();
    }

    public void clear1Data() {
        step1.clear();
    }

    public void clear2Data() {
        step2.clear();
    }

    public void clear3Data() {
        step3.clear();
    }

    public void clear4Data() {
        step4.clear();
    }

    public void clear5Data() {
        step5.clear();
    }

    @Override
    public void resetData() {

        step1.clear();
        step2.clear();
        step3.clear();
        step4.clear();
        step5.clear();
    }

}
