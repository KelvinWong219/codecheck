/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.style;

/**
 *
 * @author KelvinWongNYC
 */
public class CodeCheckStyle {
    
    public static String CLASS_TABLE = "class_table";
    public static String BUTTON_TEXT = "button_text";
    
    
    public static String BUTTON_BOX = "button_box";
    
    public static String LEFT_RIGHT = "left_right";
    
    public static String HEADER_TEXT = "header_text";
    public static String STEP_TEXT = "step_text";
    public static String CLASS_PROMPT_LABEL = "prompt_label";
    public static String TEXTAREA_TEXT = "textarea_text";
    
    public static String FILETYPE_TEXT = "filetype_text";
    public static String PROGRESS_TEXT = "progress_text";
    
}
