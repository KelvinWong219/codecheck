/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import static sc.CodeCheckProp.ABOUT_BUTTON_TEXT;
import static sc.CodeCheckProp.APP_TITLE;
import static sc.CodeCheckProp.BLACKBOARD_SUBMISSION_TABLETEXT;
import static sc.CodeCheckProp.CHCPP_TEXT;
import static sc.CodeCheckProp.CHECK_PROGRESS_TEXT;
import static sc.CodeCheckProp.CODECHECK_BUTTON_TEXT;
import static sc.CodeCheckProp.CODE_PROGRESS_TEXT;
import static sc.CodeCheckProp.CS_TEXT;
import static sc.CodeCheckProp.EXTRACTCODE_BUTTON_TEXT;
import static sc.CodeCheckProp.EXTRACT_BUTTON_TEXT;
import static sc.CodeCheckProp.EXTRACT_PROGRESS_TEXT;
import static sc.CodeCheckProp.HOME_BUTTON_TEXT;
import static sc.CodeCheckProp.JAVA_TYPE;
import static sc.CodeCheckProp.JS_TYPE;
import static sc.CodeCheckProp.LOAD_BUTTON_TEXT;
import static sc.CodeCheckProp.NEW_BUTTON_TEXT;
import static sc.CodeCheckProp.NEXT_BUTTON_TEXT;
import static sc.CodeCheckProp.PREVIOUS_BUTTON_TEXT;
import static sc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static sc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static sc.CodeCheckProp.RENAME_BUTTON;
import static sc.CodeCheckProp.RENAME_BUTTON_TEXT;
import static sc.CodeCheckProp.RENAME_PROGRESS_TEXT;
import static sc.CodeCheckProp.SOURCE_FILE_TYPE;
import static sc.CodeCheckProp.STEP1_HEADER;
import static sc.CodeCheckProp.STEP1_TEXT;
import static sc.CodeCheckProp.STEP2_HEADER;
import static sc.CodeCheckProp.STEP2_TEXT;
import static sc.CodeCheckProp.STEP3_HEADER;
import static sc.CodeCheckProp.STEP3_TEXT;
import static sc.CodeCheckProp.STEP4_HEADER;
import static sc.CodeCheckProp.STEP4_TEXT;
import static sc.CodeCheckProp.STEP5_HEADER;
import static sc.CodeCheckProp.STEP5_TEXT;
import static sc.CodeCheckProp.STUDENTWORK_TABLETEXT;
import static sc.CodeCheckProp.STUDENTWORK_TEXT;
import static sc.CodeCheckProp.STUDENT_SUBMISSION_TABLETEXT;
import static sc.CodeCheckProp.STUDENT_ZIP_TABLETEXT;
import static sc.CodeCheckProp.UNZIP_BUTTON_TEXT;
import static sc.CodeCheckProp.UNZIP_PROGRESS_TEXT;
import static sc.CodeCheckProp.VIEW_BUTTON_TEXT;
import static sc.CodeCheckProp.VIEW_RESULTS_TEXT;
import sc.data.CodeCheckData;
import sc.CodeCheckApp;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;

import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import static sc.style.CodeCheckStyle.BUTTON_BOX;
import static sc.style.CodeCheckStyle.BUTTON_TEXT;
import static sc.style.CodeCheckStyle.CLASS_PROMPT_LABEL;
import static sc.style.CodeCheckStyle.CLASS_TABLE;
import static sc.style.CodeCheckStyle.FILETYPE_TEXT;
import static sc.style.CodeCheckStyle.HEADER_TEXT;
import static sc.style.CodeCheckStyle.LEFT_RIGHT;
import static sc.style.CodeCheckStyle.STEP_TEXT;
import static sc.style.CodeCheckStyle.TEXTAREA_TEXT;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

/**
 *
 * @author KelvinWongNYC
 */
public class CodeCheckWorkspace extends AppWorkspaceComponent {

    CodeCheckApp app;
    CodeCheckController controller;
    HBox editScreenToolbar;
    HBox tools;

    Button newButton;
    Button load;
    Button rename;
    Button about;

    Button home;
    Button previous;
    Button next;

    GridPane left1;
    Label step1Header;
    Label step1Instruction;

    ScrollPane tableScrollPane1;
    TableView<File> tableView1;
    ScrollPane tableScrollPane2;
    TableView<File> tableView2;
    ScrollPane tableScrollPane3;
    TableView<File> tableView3;
    ScrollPane tableScrollPane4;
    TableView<File> tableView4;
    ScrollPane tableScrollPane5;
    TableView<File> tableView5;

    TableColumn<File, String> blackBoardSubmissionsColumn;

    GridPane right1;
    Label extractionProgress;
    Button extract;
    HBox textArea;
    TextArea updatesForUser1, updatesForUser2, updatesForUser3, updatesForUser4, updatesForUser5;
    Button remove1, refresh1, view1;
    Button remove2, refresh2, view2;
    Button remove3, refresh3, view3;
    Button remove4, refresh4, view4;
    Button remove5, refresh5, view5;

    HBox tableButtons1;
    HBox tableButtons2;
    HBox tableButtons3;
    HBox tableButtons4;
    HBox tableButtons5;
    int currentScreen = 1;
    BorderPane step1BorderPane;
    PropertiesManager props = PropertiesManager.getPropertiesManager();

    CodeCheckData data;
    ObservableList<File> selectedFiles;
    ObservableList<File> selectedFiles2;
    ObservableList<File> selectedFiles3;
    ObservableList<File> selectedFiles4;
    ObservableList<File> selectedFiles5;

    ReentrantLock progressLock = new ReentrantLock();

    public CodeCheckWorkspace(CodeCheckApp app) {
        this.app = app;
        initializeLayout();
        initializeController();
        initializeStyle();

    }

    private void loadTable1() {
        data = (CodeCheckData) app.getDataComponent();
        data.clear1Data();
        data.setCurrentFile();
        data.loadStep1Data();
        blackBoardSubmissionsColumn.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView1.setItems(data.getStep1Data());
        tableView1.refresh();
    }

    private void loadTable2() {
        data = (CodeCheckData) app.getDataComponent();
        data.clear2Data();
        data.setCurrentFile();
        data.loadStep2Data();
        studentSubmissionsColumn.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );

        tableView2.setItems(data.getStep2Data());
        if (data.getStep2Data().size() == 0) {
            renameButton.setDisable(true);
        } else {
            renameButton.setDisable(false);
        }

        tableView2.refresh();

    }

    private void loadTable3() {
        data = (CodeCheckData) app.getDataComponent();
        data.clear3Data();
        data.setCurrentFile();
        data.loadStep3Data();
        studentZipFiles.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView3.setItems(data.getStep3Data());
        tableView3.refresh();

    }

    private void loadTable4() {
        data = (CodeCheckData) app.getDataComponent();
        data.clear4Data();
        data.setCurrentFile();
        data.loadStep4Data();
        studentWorkDirectories.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView4.setItems(data.getStep4Data());
        tableView4.refresh();
    }

    private void loadTable5() {
        data = (CodeCheckData) app.getDataComponent();
        data.clear5Data();
        data.setCurrentFile();
        data.loadStep5Data();
        studentWork.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView5.setItems(data.getStep5Data());
        tableView5.refresh();

    }

    private void initializeController() {
        controller = new CodeCheckController(app);

        newButton.setOnAction(e -> {
            app.getGUI().getWindow().setTitle(props.getProperty(APP_TITLE) + " - "
                    + app.getGUI().getFileController().handleNewCodeCheck());
            app.getGUI().getFileController().handleNewRequest();
            //workspace = step1BorderPane;
            //app.getGUI().getAppPane().setCenter(step1BorderPane);
        });

        load.setOnAction(e -> {
            app.getGUI().getFileController().promptToOpen();
        });
        rename.setOnAction(e -> {
            app.getGUI().getFileController().rename();
        });

        about.setOnAction(e -> {
            controller.handleAboutRequest();
        });

        home.setOnAction(e -> {
            workspace = step1BorderPane;
            currentScreen = 1;
            previous.setDisable(true);
            next.setDisable(false);
            home.setDisable(true);
            tableView1.getSelectionModel().clearSelection();
            remove1.setDisable(true);
            view1.setDisable(true);
            extract.setDisable(true);
            loadTable1();
            app.getGUI().getAppPane().setCenter(step1BorderPane);
        });

        previous.setOnAction(e -> {
            --currentScreen;
            if (currentScreen == 1) {
                home.setDisable(true);
                previous.setDisable(true);
                next.setDisable(false);
                tableView1.getSelectionModel().clearSelection();
                remove1.setDisable(true);
                view1.setDisable(true);
                extract.setDisable(true);
                loadTable1();
                app.getGUI().getAppPane().setCenter(step1BorderPane);
            } else if (currentScreen == 2) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView2.getSelectionModel().clearSelection();
                remove2.setDisable(true);
                view2.setDisable(true);
                loadTable2();
                app.getGUI().getAppPane().setCenter(step2BorderPane);
            } else if (currentScreen == 3) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView3.getSelectionModel().clearSelection();
                remove3.setDisable(true);
                view3.setDisable(true);
                unzip.setDisable(true);
                loadTable3();
                app.getGUI().getAppPane().setCenter(step3BorderPane);
            } else if (currentScreen == 4) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView4.getSelectionModel().clearSelection();
                remove4.setDisable(true);
                view4.setDisable(true);
                extractCode.setDisable(true);
                source1.setDisable(true);
                source2.setDisable(true);
                source3.setDisable(true);
                loadTable4();
                app.getGUI().getAppPane().setCenter(step4BorderPane);
            } else if (currentScreen == 5) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(true);
                tableView5.getSelectionModel().clearSelection();
                remove5.setDisable(true);
                view5.setDisable(true);
                codeCheck.setDisable(true);
                viewResults.setDisable(true);

                loadTable5();
                app.getGUI().getAppPane().setCenter(step5BorderPane);
            }

        });

        next.setOnAction(e -> {
            ++currentScreen;
            if (currentScreen == 1) {
                home.setDisable(true);
                previous.setDisable(true);
                next.setDisable(false);
                tableView1.getSelectionModel().clearSelection();
                remove1.setDisable(true);
                view1.setDisable(true);
                extract.setDisable(true);
                loadTable1();
                app.getGUI().getAppPane().setCenter(step1BorderPane);
            } else if (currentScreen == 2) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView2.getSelectionModel().clearSelection();
                remove2.setDisable(true);
                view2.setDisable(true);
                loadTable2();
                app.getGUI().getAppPane().setCenter(step2BorderPane);
            } else if (currentScreen == 3) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView3.getSelectionModel().clearSelection();
                remove3.setDisable(true);
                view3.setDisable(true);
                unzip.setDisable(true);
                loadTable3();
                app.getGUI().getAppPane().setCenter(step3BorderPane);
            } else if (currentScreen == 4) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(false);
                tableView4.getSelectionModel().clearSelection();
                remove4.setDisable(true);
                view4.setDisable(true);
                extractCode.setDisable(true);
                source1.setDisable(true);
                source2.setDisable(true);
                source3.setDisable(true);
                loadTable4();
                app.getGUI().getAppPane().setCenter(step4BorderPane);
            } else if (currentScreen == 5) {
                home.setDisable(false);
                previous.setDisable(false);
                next.setDisable(true);
                tableView5.getSelectionModel().clearSelection();
                remove5.setDisable(true);
                view5.setDisable(true);
                codeCheck.setDisable(true);
                viewResults.setDisable(true);
                loadTable5();
                app.getGUI().getAppPane().setCenter(step5BorderPane);
            }

        });

        refresh5.setOnAction(e -> {
            loadTable5();
        });

        tableView5.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableView5.setOnMouseClicked(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                selectedFiles5 = tableView5.getSelectionModel().getSelectedItems();
            }
        });
        tableView5.getSelectionModel().selectedItemProperty().addListener((listener) -> {
            codeCheck.setDisable(false);
            remove5.setDisable(false);
            view5.setDisable(false);
        });
        /*
        codeCheck.setOnAction(e -> {
            controller.appendTextArea5(updatesForUser5);
            viewResults.setDisable(false);
        });*/

        codeCheck.setOnAction(e -> {
            Task<Void> task = new Task<Void>() {
                double max = 200;
                double percentage;

                @Override
                protected Void call() throws Exception {
                    try {
                        updatesForUser5.clear();
                        progressLock.lock();
                        for (int i = 0; i < 200; i++) {
                            percentage = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar5.setProgress(percentage);
                                    pi5.setProgress(percentage);
                                }
                            });
                            Thread.sleep(10);
                        }
                    } finally {

                        progressLock.unlock();
                        Platform.runLater(() -> controller.appendTextArea5(updatesForUser5));
                        //controller.appendTextArea5(updatesForUser5);
                        viewResults.setDisable(false);
                    }
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.start();
        });

        viewResults.setOnAction(e -> {
            
            controller.webBrowser();
            
        });

        refresh4.setOnAction(e -> {
            loadTable4();
        });

        tableView4.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableView4.setOnMouseClicked(new EventHandler<Event>() {

            @Override
            public void handle(Event event) {
                selectedFiles4 = tableView4.getSelectionModel().getSelectedItems();
            }

        });
        tableView4.getSelectionModel().selectedItemProperty().addListener((listener) -> {
            source1.setDisable(false);
            source2.setDisable(false);
            source3.setDisable(false);
            extractCode.setDisable(false);
            remove4.setDisable(false);
            view4.setDisable(false);
        });
        /*
        extractCode.setOnAction(e -> {

            controller.extractFilesScreen4(selectedFiles4, java.isSelected(), js.isSelected(), chcpp.isSelected(),
                    cs.isSelected(), custom.isSelected(), customField.getText());
            controller.appendTextArea4(updatesForUser4);

        });*/
        
        extractCode.setOnAction(e -> {
            
            Task extract4 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                  controller.extractFilesScreen4(selectedFiles4, java.isSelected(), js.isSelected(), chcpp.isSelected(),
                                cs.isSelected(), custom.isSelected(), customField.getText());
                return null;
            }
        };

        Task<Void> extractCodeStep4 = new Task<Void>() {
                double max = 200;
                double percentage;

                @Override
                protected Void call() throws Exception {
                    try {
                        
                        progressLock.lock();
                        for (int i = 0; i < 200; i++) {
                            percentage = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar4.setProgress(percentage);
                                    pi4.setProgress(percentage);
                                }
                            });
                            Thread.sleep(30);
                        }
                    } finally {

                        progressLock.unlock();
                        //controller.appendTextArea4(updatesForUser4);
                        Platform.runLater(() -> controller.appendTextArea4(updatesForUser4));

                    }
                    return null;
                }
            };
            
             new Thread(extract4).start();
            
            extract4.setOnRunning(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                new Thread(extractCodeStep4).start();
            }

           }); 
        });

        refresh3.setOnAction(e -> {
            loadTable3();
        });

        tableView3.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableView3.setOnMouseClicked(new EventHandler<Event>() {

            @Override
            public void handle(Event event) {

                selectedFiles3 = tableView3.getSelectionModel().getSelectedItems();

            }

        });
        tableView3.getSelectionModel().selectedItemProperty().addListener((listener) -> {
            unzip.setDisable(false);
            remove3.setDisable(false);
            view3.setDisable(false);
        });
        /*
        unzip.setOnAction(e -> {
            controller.extractStudentWorkScreen3(selectedFiles3);
            controller.appendTextArea3(updatesForUser3);

        });*/
        
        

        unzip.setOnAction(e -> {
             Task unzipTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                  controller.extractStudentWorkScreen3(selectedFiles3);
                return null;
            }
        };
        
        
        
        Task<Void> task3 = new Task<Void>() {
                double max = 200;
                double percentage;

                @Override
                protected Void call() throws Exception {
                    try {
                        
                        progressLock.lock();
                        for (int i = 0; i < 200; i++) {
                            percentage = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar3.setProgress(percentage);
                                    pi3.setProgress(percentage);
                                }
                            });
                            Thread.sleep(60);
                        }
                       
                    } finally {

                        progressLock.unlock();
                        Platform.runLater(() -> controller.appendTextArea3(updatesForUser3));
                        //controller.appendTextArea3(updatesForUser3);
                    }
                    return null;
                }
            };
            
            new Thread(unzipTask).start();
            
            unzipTask.setOnRunning(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                new Thread(task3).start();
            }
           
        });
             
            
           
        });

        refresh2.setOnAction(e -> {
            loadTable2();
        });

        tableView2.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableView2.setOnMouseClicked(new EventHandler<Event>() {

            @Override
            public void handle(Event event) {
                selectedFiles2 = tableView2.getSelectionModel().getSelectedItems();
            }

        });

        tableView2.getSelectionModel().selectedItemProperty().addListener((listener) -> {
            remove2.setDisable(false);
            view2.setDisable(false);
        });
        /*
        renameButton.setOnAction(e -> {
            controller.handleRenameRequest(data.getStep2Data());
            controller.appendTextArea2(updatesForUser2);
        });*/
        
         

        renameButton.setOnAction(e -> {
            
            Task renameTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                 controller.handleRenameRequest(data.getStep2Data());
                return null;
            }
        };
        
        Task<Void> taskScreen2 = new Task<Void>() {
                double max = 200;
                double percentage;

                @Override
                protected Void call() throws Exception {
                    try {
                        progressLock.lock();
                        for (int i = 0; i < 200; i++) {
                            percentage = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar2.setProgress(percentage);
                                    pi2.setProgress(percentage);
                                }
                            });
                            Thread.sleep(15);
                        }
                    } finally {

                        progressLock.unlock();
                        Platform.runLater(() -> controller.appendTextArea2(updatesForUser2));
                        //controller.appendTextArea2(updatesForUser2);
                    }
                    return null;
                }
            };   
            new Thread(renameTask).start();
            
             renameTask.setOnRunning(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                new Thread(taskScreen2).start();
            }
        });
        });

        refresh1.setOnAction(e -> {
            loadTable1();
        });

        tableView1.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableView1.setOnMouseClicked(new EventHandler<Event>() {

            @Override
            public void handle(Event event) {
                selectedFiles = tableView1.getSelectionModel().getSelectedItems();
            }
        });

        tableView1.getSelectionModel().selectedItemProperty().addListener((listener) -> {
            extract.setDisable(false);
            remove1.setDisable(false);
            view1.setDisable(false);
        });
        /*
        extract.setOnAction(e -> {
            controller.handleExtractRequest(selectedFiles, updatesForUser1);
            controller.appendTextArea1(updatesForUser1);
        });*/

  
        extract.setOnAction(e -> {
             Task extractScreen1 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                controller.handleExtractRequest(selectedFiles, updatesForUser1);
                return null;
            }
        };
            
             Task<Void> taskScreen1 = new Task<Void>() {
                double max = 200;
                double percentage;

                @Override
                protected Void call() throws Exception {
                    try {
                        progressLock.lock();
                        for (int i = 0; i < 200; i++) {
                            percentage = i / max;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar1.setProgress(percentage);
                                    pi1.setProgress(percentage);
                                }
                            });
                            Thread.sleep(20);
                        }
                    } finally {
                        progressLock.unlock();
                        Platform.runLater(() -> controller.appendTextArea1(updatesForUser1));
                        //controller.appendTextArea1(updatesForUser1);
                    }
                    return null;
                }
            };
            
            Thread extract = new Thread(extractScreen1);
            extract.start();
            
            
            extractScreen1.setOnRunning(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                new Thread(taskScreen1).start();
            }
            
        });
 
        });

        remove1.setOnAction(e -> {
            controller.removeFiles(selectedFiles);
            tableView1.getSelectionModel().clearSelection();
            remove1.setDisable(true);
            view1.setDisable(true);
            extract.setDisable(true);
            loadTable1();
        });

        view1.setOnAction(e -> {
            controller.viewFile(selectedFiles);
        });

        view2.setOnAction(e -> {
            controller.viewFile(selectedFiles2);
        });

        view3.setOnAction(e -> {
            controller.viewFile(selectedFiles3);
        });

        view4.setOnAction(e -> {
            controller.viewFile(selectedFiles4);
        });

        view5.setOnAction(e -> {
            controller.viewFile(selectedFiles5);
        });

        remove2.setOnAction(e -> {
            controller.removeFiles(selectedFiles2);
            tableView2.getSelectionModel().clearSelection();
            remove2.setDisable(true);
            view2.setDisable(true);
            loadTable2();
        });

        remove3.setOnAction(e -> {
            controller.removeFiles(selectedFiles3);
            tableView3.getSelectionModel().clearSelection();
            remove3.setDisable(true);
            view3.setDisable(true);
            unzip.setDisable(true);
            loadTable3();
        });

        remove4.setOnAction(e -> {
            controller.removeFiles(selectedFiles4);
            tableView4.getSelectionModel().clearSelection();
            remove4.setDisable(true);
            view4.setDisable(true);
            extractCode.setDisable(true);
            source1.setDisable(true);
            source2.setDisable(true);
            source3.setDisable(true);
            loadTable4();
        });

        remove5.setOnAction(e -> {
            controller.removeFiles(selectedFiles5);
            tableView5.getSelectionModel().clearSelection();
            remove5.setDisable(true);
            view5.setDisable(true);
            codeCheck.setDisable(true);
            viewResults.setDisable(true);
            loadTable5();
        });

    }

    private void initializeLayout() {
        editScreenToolbar = new HBox();
        home = new Button(props.getProperty(HOME_BUTTON_TEXT));
        home.setDisable(true);
        previous = new Button(props.getProperty(PREVIOUS_BUTTON_TEXT));
        previous.setDisable(true);
        next = new Button(props.getProperty(NEXT_BUTTON_TEXT));

        editScreenToolbar.getChildren().add(home);
        editScreenToolbar.getChildren().add(previous);
        editScreenToolbar.getChildren().add(next);

        tools = new HBox();
        newButton = new Button(props.getProperty(NEW_BUTTON_TEXT));
        load = new Button(props.getProperty(LOAD_BUTTON_TEXT));
        rename = new Button(props.getProperty(RENAME_BUTTON));
        about = new Button(props.getProperty(ABOUT_BUTTON_TEXT));
        tools.getChildren().add(newButton);
        tools.getChildren().add(load);
        tools.getChildren().add(rename);
        tools.getChildren().add(about);

        app.getGUI().getTopToolbarPane().getChildren().remove(0);
        app.getGUI().getTopToolbarPane().getChildren().add(tools);
        app.getGUI().getTopToolbarPane().getChildren().add(editScreenToolbar);

        step1Initialize();
        step2Initialize();
        step3Initializer();
        step4Initializer();
        step5Initializer();

        workspace = step1BorderPane;

    }

    Label step5Header;
    Label step5Description;
    GridPane left5;
    GridPane right5;
    TableColumn<File, String> studentWork;
    Label checkProgress;
    Button codeCheck;
    Button viewResults;
    BorderPane step5BorderPane;
    ProgressBar progressBar5;
    ProgressIndicator pi5;

    private void step5Initializer() {
        step5Header = new Label(props.getProperty(STEP5_HEADER));
        step5Description = new Label(props.getProperty(STEP5_TEXT));

        tableScrollPane5 = new ScrollPane();
        tableView5 = new TableView();

        remove5 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refresh5 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        view5 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        tableButtons5 = new HBox();
        remove5.setDisable(true);
        view5.setDisable(true);
        tableButtons5.getChildren().addAll(remove5, refresh5, view5);

        updatesForUser5 = new TextArea();
        updatesForUser5.setEditable(false);
        textArea = new HBox();
        textArea.getChildren().add(updatesForUser5);

        studentWork = new TableColumn(props.getProperty(STUDENTWORK_TABLETEXT));
        studentWork.prefWidthProperty().bind(tableView5.widthProperty());
        tableView5.getColumns().clear();
        tableView5.getColumns().add(studentWork);
        tableScrollPane5.setContent(tableView5);
        tableScrollPane5.setFitToHeight(true);
        tableScrollPane5.setFitToWidth(true);

        left5 = new GridPane();
        left5.add(step5Header, 0, 0);
        left5.add(step5Description, 0, 1);
        left5.add(tableScrollPane5, 0, 2);
        left5.add(tableButtons5, 0, 3);

        checkProgress = new Label(props.getProperty(CHECK_PROGRESS_TEXT));

        HBox progress = new HBox();
        progress.getChildren().add(checkProgress);

        progressBar5 = new ProgressBar(0);
        pi5 = new ProgressIndicator(0);
        progress.getChildren().add(progressBar5);
        progress.getChildren().add(pi5);
        progress.setSpacing(50);
        progress.setAlignment(Pos.CENTER_LEFT);

        codeCheck = new Button(props.getProperty(CODECHECK_BUTTON_TEXT));
        viewResults = new Button(props.getProperty(VIEW_RESULTS_TEXT));

        codeCheck.setDisable(true);
        viewResults.setDisable(true);
        right5 = new GridPane();
        right5.add(progress, 0, 0);

        HBox step5Buttons = new HBox();
        step5Buttons.getChildren().add(codeCheck);
        step5Buttons.getChildren().add(viewResults);

        right5.add(step5Buttons, 0, 1);

        right5.add(textArea, 0, 2);

        step5BorderPane = new BorderPane();

        left5.setAlignment(Pos.TOP_CENTER);
        right5.setAlignment(Pos.TOP_CENTER);

        step5BorderPane.setCenter(left5);
        step5BorderPane.setRight(right5);

    }

    Label step4Header;
    Label step4Description;
    GridPane left4;
    GridPane right4;
    TableColumn<File, String> studentWorkDirectories;
    Label sourceFileTypes;
    CheckBox java;
    CheckBox js;
    CheckBox chcpp;
    CheckBox cs;
    CheckBox custom;
    TextField customField;

    HBox source1, source2, source3;

    Label codeProgress;
    Button extractCode;
    BorderPane step4BorderPane;
    ProgressBar progressBar4;
    ProgressIndicator pi4;

    private void step4Initializer() {

        left4 = new GridPane();
        step4Header = new Label(props.getProperty(STEP4_HEADER));
        step4Description = new Label(props.getProperty(STEP4_TEXT));

        remove4 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refresh4 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        view4 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        tableButtons4 = new HBox();
        remove4.setDisable(true);
        view4.setDisable(true);
        tableButtons4.getChildren().addAll(remove4, refresh4, view4);

        updatesForUser4 = new TextArea();
        updatesForUser4.setEditable(false);
        textArea = new HBox();
        textArea.getChildren().add(updatesForUser4);

        tableScrollPane4 = new ScrollPane();
        tableView4 = new TableView();

        studentWorkDirectories = new TableColumn(props.getProperty(STUDENTWORK_TEXT));
        studentWorkDirectories.prefWidthProperty().bind(tableView4.widthProperty());

        tableView4.getColumns().add(studentWorkDirectories);
        tableScrollPane4.setContent(tableView4);
        tableScrollPane4.setFitToHeight(true);
        tableScrollPane4.setFitToWidth(true);
        sourceFileTypes = new Label(props.getProperty(SOURCE_FILE_TYPE));

        left4.add(step4Header, 0, 0);
        left4.add(step4Description, 0, 1);
        left4.add(tableScrollPane4, 0, 2);
        left4.add(tableButtons4, 0, 3);
        left4.add(sourceFileTypes, 0, 4);

        java = new CheckBox(props.getProperty(JAVA_TYPE));
        js = new CheckBox(props.getProperty(JS_TYPE));
        chcpp = new CheckBox(props.getProperty(CHCPP_TEXT));
        cs = new CheckBox(props.getProperty(CS_TEXT));
        custom = new CheckBox();
        customField = new TextField();

        source1 = new HBox();
        source1.setSpacing(100);
        source1.getChildren().add(java);
        source1.getChildren().add(js);
        source1.setDisable(true);
        left4.add(source1, 0, 5);

        source2 = new HBox();
        source2.setSpacing(40);
        source2.getChildren().add(chcpp);
        source2.getChildren().add(cs);
        source2.setDisable(true);
        left4.add(source2, 0, 6);

        source3 = new HBox();
        source3.getChildren().add(custom);
        source3.getChildren().add(customField);
        source3.setDisable(true);
        left4.add(source3, 0, 7);

        right4 = new GridPane();

        codeProgress = new Label(props.getProperty(CODE_PROGRESS_TEXT));
        extractCode = new Button(props.getProperty(EXTRACTCODE_BUTTON_TEXT));
        extractCode.setDisable(true);

        HBox progress = new HBox();
        progress.getChildren().add(codeProgress);

        progressBar4 = new ProgressBar(0);
        pi4 = new ProgressIndicator(0);
        progress.getChildren().add(progressBar4);
        progress.getChildren().add(pi4);
        progress.setSpacing(50);
        progress.setAlignment(Pos.CENTER_LEFT);

        right4.add(progress, 0, 0);
        right4.add(extractCode, 0, 1);
        right4.add(textArea, 0, 2);

        step4BorderPane = new BorderPane();

        left4.setAlignment(Pos.TOP_CENTER);
        right4.setAlignment(Pos.TOP_CENTER);

        step4BorderPane.setCenter(left4);
        step4BorderPane.setRight(right4);

    }

    Label step3Header;
    Label step3Description;
    GridPane left3;
    GridPane right3;
    TableColumn<File, String> studentZipFiles;
    Label UnzipProgress;
    Button unzip;
    BorderPane step3BorderPane;
    ProgressBar progressBar3;
    ProgressIndicator pi3;

    private void step3Initializer() {
        left3 = new GridPane();
        step3Header = new Label(props.getProperty(STEP3_HEADER));
        step3Description = new Label(props.getProperty(STEP3_TEXT));

        remove3 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refresh3 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        view3 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        tableButtons3 = new HBox();
        remove3.setDisable(true);
        view3.setDisable(true);
        tableButtons3.getChildren().addAll(remove3, refresh3, view3);

        updatesForUser3 = new TextArea();
        updatesForUser3.setEditable(false);
        textArea = new HBox();
        textArea.getChildren().add(updatesForUser3);

        tableScrollPane3 = new ScrollPane();
        tableView3 = new TableView();

        studentZipFiles = new TableColumn(props.getProperty(STUDENT_ZIP_TABLETEXT));
        studentZipFiles.prefWidthProperty().bind(tableView3.widthProperty());

        tableView3.getColumns().add(studentZipFiles);
        tableScrollPane3.setContent(tableView3);
        tableScrollPane3.setFitToHeight(true);
        tableScrollPane3.setFitToWidth(true);

        left3.add(step3Header, 0, 0);
        left3.add(step3Description, 0, 1);
        left3.add(tableScrollPane3, 0, 2);
        left3.add(tableButtons3, 0, 3);

        right3 = new GridPane();

        UnzipProgress = new Label(props.getProperty(UNZIP_PROGRESS_TEXT));
        unzip = new Button(props.getProperty(UNZIP_BUTTON_TEXT));
        unzip.setDisable(true);

        HBox progress = new HBox();
        progress.getChildren().add(UnzipProgress);

        progressBar3 = new ProgressBar(0);
        pi3 = new ProgressIndicator(0);
        progress.getChildren().add(progressBar3);
        progress.getChildren().add(pi3);
        progress.setSpacing(50);
        progress.setAlignment(Pos.CENTER_LEFT);

        right3.add(progress, 0, 0);
        right3.add(unzip, 0, 1);
        right3.add(textArea, 0, 2);

        step3BorderPane = new BorderPane();
        left3.setAlignment(Pos.TOP_CENTER);
        right3.setAlignment(Pos.TOP_CENTER);

        step3BorderPane.setRight(right3);
        step3BorderPane.setCenter(left3);

    }

    Label step2Header;
    Label step2Description;
    GridPane left2;
    GridPane right2;
    TableColumn<File, String> studentSubmissionsColumn;
    Label renameProgress;
    Button renameButton;
    BorderPane step2BorderPane;
    ProgressBar progressBar2;
    ProgressIndicator pi2;

    private void step2Initialize() {

        step2BorderPane = new BorderPane();
        left2 = new GridPane();
        step2Header = new Label(props.getProperty(STEP2_HEADER));
        step2Description = new Label(props.getProperty(STEP2_TEXT));

        remove2 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refresh2 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        view2 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        tableButtons2 = new HBox();

        remove2.setDisable(true);
        view2.setDisable(true);
        tableButtons2.getChildren().addAll(remove2, refresh2, view2);

        updatesForUser2 = new TextArea();
        updatesForUser2.setEditable(false);
        textArea = new HBox();
        textArea.getChildren().add(updatesForUser2);

        tableScrollPane2 = new ScrollPane();
        tableView2 = new TableView();

        studentSubmissionsColumn = new TableColumn(props.getProperty(STUDENT_SUBMISSION_TABLETEXT));
        studentSubmissionsColumn.prefWidthProperty().bind(tableView2.widthProperty());

        tableView2.getColumns().add(studentSubmissionsColumn);
        tableScrollPane2.setContent(tableView2);

        tableScrollPane2.setFitToHeight(true);
        tableScrollPane2.setFitToWidth(true);

        left2.add(step2Header, 0, 0);
        left2.add(step2Description, 0, 1);
        left2.add(tableScrollPane2, 0, 2);
        left2.add(tableButtons2, 0, 3);

        renameProgress = new Label(props.getProperty(RENAME_PROGRESS_TEXT));
        renameButton = new Button(props.getProperty(RENAME_BUTTON_TEXT));

        HBox progress = new HBox();
        progress.getChildren().add(renameProgress);

        progressBar2 = new ProgressBar(0);
        pi2 = new ProgressIndicator(0);
        progress.getChildren().add(progressBar2);
        progress.getChildren().add(pi2);
        progress.setSpacing(50);
        progress.setAlignment(Pos.CENTER_LEFT);

        right2 = new GridPane();
        right2.add(progress, 0, 0);
        right2.add(renameButton, 0, 1);
        right2.add(textArea, 0, 2);

        left2.setAlignment(Pos.TOP_CENTER);
        right2.setAlignment(Pos.TOP_CENTER);

        step2BorderPane.setRight(right2);
        step2BorderPane.setCenter(left2);

    }
    ProgressBar progressBar1;
    ProgressIndicator pi1;

    private void step1Initialize() {

        step1BorderPane = new BorderPane();

        remove1 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        refresh1 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        view1 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        tableButtons1 = new HBox();
        remove1.setDisable(true);
        view1.setDisable(true);
        tableButtons1.getChildren().addAll(remove1, refresh1, view1);

        updatesForUser1 = new TextArea();
        updatesForUser1.setEditable(false);
        textArea = new HBox();
        textArea.getChildren().add(updatesForUser1);

        left1 = new GridPane();
        step1Header = new Label(props.getProperty(STEP1_HEADER));
        step1Instruction = new Label(props.getProperty(STEP1_TEXT));

        tableScrollPane1 = new ScrollPane();
        tableView1 = new TableView();
        blackBoardSubmissionsColumn = new TableColumn(props.getProperty(BLACKBOARD_SUBMISSION_TABLETEXT));
        blackBoardSubmissionsColumn.prefWidthProperty().bind(tableView1.widthProperty());
        tableView1.getColumns().add(blackBoardSubmissionsColumn);

        tableScrollPane1.setContent(tableView1);

        tableScrollPane1.setFitToHeight(true);
        tableScrollPane1.setFitToWidth(true);

        left1.add(step1Header, 0, 0);
        left1.add(step1Instruction, 0, 1);
        left1.add(tableScrollPane1, 0, 2);
        left1.add(tableButtons1, 0, 3);

        right1 = new GridPane();
        extractionProgress = new Label(props.getProperty(EXTRACT_PROGRESS_TEXT));

        extract = new Button(props.getProperty(EXTRACT_BUTTON_TEXT));
        extract.setDisable(true);

        HBox progress = new HBox();
        progress.getChildren().add(extractionProgress);

        progressBar1 = new ProgressBar(0);
        pi1 = new ProgressIndicator(0);
        progress.getChildren().add(progressBar1);
        progress.getChildren().add(pi1);
        progress.setSpacing(50);
        progress.setAlignment(Pos.CENTER_LEFT);

        right1.add(progress, 0, 0);
        right1.add(extract, 0, 1);
        right1.add(textArea, 0, 2);

        left1.setAlignment(Pos.TOP_CENTER);
        right1.setAlignment(Pos.TOP_CENTER);
        step1BorderPane.setCenter(left1);
        step1BorderPane.setRight(right1);

    }

    private void initializeStyle() {
        tools.getStyleClass().add(CLASS_BORDERED_PANE);
        tableButtons1.getStyleClass().add(BUTTON_BOX);
        remove1.getStyleClass().add(BUTTON_TEXT);
        refresh1.getStyleClass().add(BUTTON_TEXT);
        view1.getStyleClass().add(BUTTON_TEXT);

        tableButtons2.getStyleClass().add(BUTTON_BOX);
        remove2.getStyleClass().add(BUTTON_TEXT);
        refresh2.getStyleClass().add(BUTTON_TEXT);
        view2.getStyleClass().add(BUTTON_TEXT);

        tableButtons3.getStyleClass().add(BUTTON_BOX);
        remove3.getStyleClass().add(BUTTON_TEXT);
        refresh3.getStyleClass().add(BUTTON_TEXT);
        view3.getStyleClass().add(BUTTON_TEXT);

        tableButtons4.getStyleClass().add(BUTTON_BOX);
        remove4.getStyleClass().add(BUTTON_TEXT);
        refresh4.getStyleClass().add(BUTTON_TEXT);
        view4.getStyleClass().add(BUTTON_TEXT);

        tableButtons5.getStyleClass().add(BUTTON_BOX);
        remove5.getStyleClass().add(BUTTON_TEXT);
        refresh5.getStyleClass().add(BUTTON_TEXT);
        view5.getStyleClass().add(BUTTON_TEXT);

        tableView1.getStyleClass().add(CLASS_TABLE);
        tableView2.getStyleClass().add(CLASS_TABLE);
        tableView3.getStyleClass().add(CLASS_TABLE);
        tableView4.getStyleClass().add(CLASS_TABLE);
        tableView5.getStyleClass().add(CLASS_TABLE);

        editScreenToolbar.getStyleClass().add(CLASS_BORDERED_PANE);

        step1Header.getStyleClass().add(HEADER_TEXT);
        step1Instruction.getStyleClass().add(STEP_TEXT);
        left1.getStyleClass().add(LEFT_RIGHT);
        right1.getStyleClass().add(LEFT_RIGHT);
        extractionProgress.getStyleClass().add(STEP_TEXT);
        extract.getStyleClass().add(BUTTON_TEXT);
        textArea.getStyleClass().add(TEXTAREA_TEXT);

        step2Header.getStyleClass().add(HEADER_TEXT);
        step2Description.getStyleClass().add(STEP_TEXT);
        left2.getStyleClass().add(LEFT_RIGHT);
        right2.getStyleClass().add(LEFT_RIGHT);
        renameButton.getStyleClass().add(BUTTON_TEXT);
        renameProgress.getStyleClass().add(STEP_TEXT);

        step3Header.getStyleClass().add(HEADER_TEXT);
        step3Description.getStyleClass().add(STEP_TEXT);
        left3.getStyleClass().add(LEFT_RIGHT);
        right3.getStyleClass().add(LEFT_RIGHT);
        unzip.getStyleClass().add(BUTTON_TEXT);
        UnzipProgress.getStyleClass().add(STEP_TEXT);

        step4Header.getStyleClass().add(HEADER_TEXT);
        step4Description.getStyleClass().add(STEP_TEXT);
        left4.getStyleClass().add(LEFT_RIGHT);
        right4.getStyleClass().add(LEFT_RIGHT);
        sourceFileTypes.getStyleClass().add(STEP_TEXT);

        codeProgress.getStyleClass().add(STEP_TEXT);
        extractCode.getStyleClass().add(BUTTON_TEXT);
        java.getStyleClass().add(FILETYPE_TEXT);
        js.getStyleClass().add(FILETYPE_TEXT);
        chcpp.getStyleClass().add(FILETYPE_TEXT);
        cs.getStyleClass().add(FILETYPE_TEXT);
        custom.getStyleClass().add(FILETYPE_TEXT);
        customField.getStyleClass().add(FILETYPE_TEXT);

        step5Header.getStyleClass().add(HEADER_TEXT);
        step5Description.getStyleClass().add(STEP_TEXT);
        left5.getStyleClass().add(LEFT_RIGHT);
        right5.getStyleClass().add(LEFT_RIGHT);
        checkProgress.getStyleClass().add(STEP_TEXT);
        codeCheck.getStyleClass().add(BUTTON_TEXT);
        viewResults.getStyleClass().add(BUTTON_TEXT);

    }

    @Override
    public void resetWorkspace() {
        //STEP 1  
        tableView1.getSelectionModel().clearSelection();
        remove1.setDisable(true);
        view1.setDisable(true);

        extract.setDisable(true);
        progressBar1.setProgress(0);
        pi1.setProgress(0);
        updatesForUser1.clear();

        //STEP 2
        tableView2.getSelectionModel().clearSelection();
        remove2.setDisable(true);
        view2.setDisable(true);
        progressBar2.setProgress(0);
        pi2.setProgress(0);
        updatesForUser2.clear();

        //STEP 3
        tableView3.getSelectionModel().clearSelection();
        remove3.setDisable(true);
        view3.setDisable(true);
        
        unzip.setDisable(true);
        progressBar3.setProgress(0);
        pi3.setProgress(0);
        updatesForUser3.clear();

        //STEP 4
        tableView4.getSelectionModel().clearSelection();
        remove4.setDisable(true);
        view4.setDisable(true);
        progressBar4.setProgress(0);
        pi4.setProgress(0);
        updatesForUser4.clear();
        extractCode.setDisable(true);
        
        java.setSelected(false);
        js.setSelected(false);
        chcpp.setSelected(false);
        cs.setSelected(false);
        custom.setSelected(false);
        customField.clear();
        source1.setDisable(true);
        source2.setDisable(true);
        source3.setDisable(true);
        
        //STEP 5
        tableView5.getSelectionModel().clearSelection();
        remove5.setDisable(true);
        view5.setDisable(true);
        
        codeCheck.setDisable(true);
        viewResults.setDisable(true);
        progressBar5.setProgress(0);
        pi5.setProgress(0);
        updatesForUser5.clear();

    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        data = (CodeCheckData) app.getDataComponent();
        data.setCurrentFile();
        data.loadStep1Data();
        blackBoardSubmissionsColumn.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView1.setItems(data.getStep1Data());
        tableView1.refresh();

        data.loadStep2Data();
        studentSubmissionsColumn.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView2.setItems(data.getStep2Data());

        if (data.getStep2Data().size() == 0) {
            renameButton.setDisable(true);
        } else {
            renameButton.setDisable(false);
        }

        tableView2.refresh();

        data.loadStep3Data();
        studentZipFiles.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView3.setItems(data.getStep3Data());
        tableView3.refresh();

        data.loadStep4Data();
        studentWorkDirectories.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView4.setItems(data.getStep4Data());
        tableView4.refresh();

        data.loadStep5Data();
        studentWork.setCellValueFactory(
                f -> new ReadOnlyStringWrapper(f.getValue().getName())
        );
        tableView5.setItems(data.getStep5Data());
        tableView5.refresh();

        workspace = step1BorderPane;
    }

}
