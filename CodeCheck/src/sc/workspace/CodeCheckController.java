/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

import static sc.CodeCheckProp.APP_AUTHOR;
import static sc.CodeCheckProp.APP_TITLE;
import static sc.CodeCheckProp.APP_YEAR;
import static djf.settings.AppPropertyType.BUTTON_NO;
import static djf.settings.AppPropertyType.BUTTON_YES;
import static djf.settings.AppPropertyType.CLOSING_CODE_CHECK_TITLE;
import static djf.settings.AppPropertyType.CLOSING_CONFIRMATION;
import static djf.settings.AppPropertyType.CLOSING_CONTEXT;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import properties_manager.PropertiesManager;
import sc.CodeCheckApp;

/**
 *
 * @author KelvinWongNYC
 */
public class CodeCheckController {

    CodeCheckApp app;

    public CodeCheckController(CodeCheckApp app) {
        this.app = app;
    }

    public void appendTextArea5(TextArea updatesForUser5) {
        updatesForUser5.clear();
        updatesForUser5.appendText("Student Palgarism Check results can be found at https://www.google.com/");

    }

    public void extractFilesScreen4(ObservableList<File> selectedFiles4, boolean java, boolean js, boolean chcpp, boolean cs,
            boolean custom, String textField) {
        for (int i = 0; i < selectedFiles4.size(); i++) {
            String directoryName = selectedFiles4.get(i).getName();
            (new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/code/" + directoryName)).mkdir();
            File destination = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/code/" + directoryName + "/");
            File currentFile = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/projects/" + directoryName);
            System.out.println("Destination " + destination.getPath());
            if (java) {
                getCode(".java", destination, currentFile);

            }
            if (js) {
                getCode(".js", destination, currentFile);
            }
            if (chcpp) {
                getCode(".c", destination, currentFile);
                getCode(".h", destination, currentFile);
                getCode(".cpp", destination, currentFile);

            }

            if (cs) {
                getCode(".cs", destination, currentFile);
            }

            if (custom && !textField.trim().equals("")) {
                getCode(textField.toLowerCase(), destination, currentFile);
            }
        }
    }

    public void getCode(String fileExtension, File destination, File currentFile) {

        File[] sub = currentFile.listFiles();
        for (File file : sub) {
            if (file.isDirectory()) {
                getCode(fileExtension, destination, file);
            } else if (file.getName().toLowerCase().endsWith(fileExtension)) {

                //Files.copy(file.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
                boolean moveFile = file.renameTo(new File(destination.getAbsolutePath() + "/" + file.getName()));
                System.out.println("Move file" + moveFile);
            } else {

            }

        }

    }

    public void appendTextArea4(TextArea updatesForUser4) {
        updatesForUser4.clear();
        updatesForUser4.appendText("Successful code extraction:\n");
        File destination = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/code/");
        File[] sub = destination.listFiles();
        for (int i = 0; i < sub.length; i++) {
            if (sub[i].isDirectory()) {
                updatesForUser4.appendText("-" + sub[i].getName() + "\n");

                File subDirectory = new File(sub[i].getAbsolutePath());
                File[] s = subDirectory.listFiles();

                for (int j = 0; j < s.length; j++) {
                    if(!s[j].isHidden()){
                        updatesForUser4.appendText("---" + s[j].getName() + "\n");
                    }
                }

            }
        }

    }

    public void extractStudentWorkScreen3(ObservableList<File> selectedFiles3) {
        try {
            for (int i = 0; i < selectedFiles3.size(); i++) {
                ZipFile zipFile = new ZipFile(selectedFiles3.get(i).getAbsolutePath());
                String zipFileName = selectedFiles3.get(i).getName().substring(0, selectedFiles3.get(i).getName().toLowerCase().indexOf(".zip"));
                (new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/projects/" + zipFileName)).mkdir();
                File f = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/projects/" + zipFileName);
                zipFile.extractAll(f.getAbsolutePath());
                success3.add(selectedFiles3.get(i).getName());
            }
        } catch (ZipException ex) {
            System.out.println("Cannot extract files");
            Logger.getLogger(CodeCheckWorkspace.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    ArrayList<String> success3 = new ArrayList<>();

    public void appendTextArea3(TextArea updatesForUser3) {

        updatesForUser3.clear();
        updatesForUser3.appendText("Successfully unzipped files:\n");
        for (int i = 0; i < success3.size(); i++) {
            updatesForUser3.appendText("-" + success3.get(i) + "\n");
        }
        success3.clear();
    }

    public void handleExtractRequest(ObservableList<File> selectedFiles, TextArea updatesForUser1) {
        try {
            for (int i = 0; i < selectedFiles.size(); i++) {
                ZipFile zipFile = new ZipFile(selectedFiles.get(i).getAbsolutePath());
                File f = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/submissions");
                zipFile.extractAll(f.getAbsolutePath());
            }

        } catch (ZipException ex) {
            Logger.getLogger(CodeCheckWorkspace.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    ArrayList<String> oldName = new ArrayList<>();
    ArrayList<String> newNameList = new ArrayList<>();
    ArrayList<String> errors = new ArrayList<>();

    public void handleRenameRequest(ObservableList<File> selectedFiles2) {
        for (int i = 0; i < selectedFiles2.size(); i++) {

            String originalName = selectedFiles2.get(i).getName();
            if (originalName.indexOf("_") != -1 && originalName.indexOf("attempt") - 1 != -1) {
                String newName = originalName.substring(originalName.indexOf("_") + 1, originalName.indexOf("attempt") - 1) + ".zip";
                File f = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/submissions/" + newName);
                if (selectedFiles2.get(i).renameTo(f)) {
                    oldName.add(originalName);
                    newNameList.add(newName);

                } else {
                    errors.add(originalName);
                }

            } else {
                errors.add(originalName);
            }

        }
    }

    public void appendTextArea2(TextArea updatesForUser2) {
        updatesForUser2.clear();
        updatesForUser2.appendText("Successfully renamed submissions:\n");
        if (oldName.size() == 0 && newNameList.size() == 0) {
            updatesForUser2.appendText("-none\n");
        } else {
            for (int i = 0; i < oldName.size(); i++) {
                updatesForUser2.appendText("-" + oldName.get(i) + " becomes " + newNameList.get(i) + "\n");
            }
        }

        updatesForUser2.appendText("\nRename Errors:\n");
        if (errors.size() == 0) {
            updatesForUser2.appendText("-none");
        } else {
            for (int i = 0; i < errors.size(); i++) {
                updatesForUser2.appendText("-" + errors.get(i) + "\n");
            }
        }
        oldName.clear();
        newNameList.clear();
        errors.clear();

    }

    public void appendTextArea1(TextArea updatesForUser1) {
        updatesForUser1.clear();
        updatesForUser1.appendText("Successfully extracted files:\n");
        File f = new File("work/" + app.getGUI().getFileController().getCurrentFile().getName() + "/submissions");
        File[] subFiles = f.listFiles();
        for (File file : subFiles) {
            if (file.getName().toLowerCase().contains(".zip")) {
                updatesForUser1.appendText("-" + file.getName() + "\n");
            }
        }

        updatesForUser1.appendText("\nSubmission Errors:\n");
        boolean badSubmission = false;
        for (File file : subFiles) {
            if (!file.isHidden()) {
                if (file.getName().toLowerCase().contains(".txt") || file.getName().toLowerCase().contains(".zip")) {

                } else {
                    updatesForUser1.appendText(file.getName() + "\n");
                    badSubmission = true;
                }
            }

        }
        if (!badSubmission) {
            updatesForUser1.appendText("-none");
        }

    }

    public void removeFiles(ObservableList<File> selectedFiles) {

        for (int i = 0; i < selectedFiles.size(); i++) {

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete File");
            alert.setHeaderText("Are you sure you want to delete " + selectedFiles.get(i).getName() + " ?");
            alert.setContentText("Please select an option");
            ButtonType yes = new ButtonType("Yes");
            ButtonType no = new ButtonType("No");
            alert.getButtonTypes().setAll(yes, no);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == yes) {
                try {
                    if (selectedFiles.get(i).isDirectory()) {

                        helperRemove(selectedFiles.get(i));
                    } else {
                        Files.delete(selectedFiles.get(i).toPath());
                    }
                } catch (IOException ex) {
                    System.out.println("Unable to delete file");
                }
            } else if (result.get() == no) {
                continue;
            }
        }
    }

    public void helperRemove(File f) throws IOException {
        File[] subFiles = f.listFiles();
        if (subFiles != null) {
            for (File file : subFiles) {
                helperRemove(file);
            }
        }
        Files.delete(f.toPath());

    }

    public void viewFile(ObservableList<File> selectedFiles) {

        for (int i = 0; i < selectedFiles.size(); i++) {

            StackPane pane = new StackPane();
            TextArea text = new TextArea();
           
            text.setEditable(false);
            
            //Update Text here
            if(selectedFiles.get(i).isDirectory()){
            helperView(selectedFiles.get(i),text);
            }else{
                viewZip(selectedFiles.get(i),text);
 
            }
 
            pane.getChildren().add(text);
            pane.setAlignment(Pos.CENTER);
            Scene scene = new Scene(pane);
            Stage stage = new Stage();
            stage.setWidth(550);
            stage.setHeight(200);
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();
            stage.setX((bounds.getWidth() - stage.getWidth()) / 2);
            stage.setY((bounds.getHeight() - stage.getHeight()) / 2);
            stage.setTitle(selectedFiles.get(i).getName());
            stage.setScene(scene);
            stage.show();
             text.setScrollTop(0);
        }

    }
    
    public void viewZip(File f, TextArea update){
        
        try {		
		ZipFile zip = new ZipFile(f.getAbsolutePath());
               
		List fileLists = zip.getFileHeaders();
	
		for (int i = 0; i < fileLists.size(); i++) {
                    FileHeader fileHeader = (FileHeader)fileLists.get(i);
                    
                   if(fileHeader.getFileName().contains(".")){
                   //Display 
                   if(fileHeader.getFileName().contains("/")){
                   update.appendText("\t"+ fileHeader.getFileName().substring(fileHeader.getFileName().lastIndexOf("/"))+"\n");
                   }else if(fileHeader.getFileName().contains("\\") ){
                   update.appendText("\t"+ fileHeader.getFileName().substring(fileHeader.getFileName().lastIndexOf("\\"))+"\n");
                   }else{
                       update.appendText(fileHeader.getFileName()+"\n");
                   }
                   
                   
                   }else{
                   update.appendText(fileHeader.getFileName()+"\n");
                   }
		}
			
		} catch (ZipException e) {
			e.printStackTrace();
		}
    }
    
    
    public void helperView(File f, TextArea update){
        File[] sub = f.listFiles();
        for(File file : sub){
            if(file.isDirectory()){
               
                try {
                    update.appendText("."+file.getAbsolutePath().substring(file.getCanonicalPath().indexOf("work")) +"\n"); 
                    
                } catch (IOException ex) {
                    Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                helperView(file, update);
            }else{
                if(!file.isHidden()){
                    update.appendText( "\t" + file.getName()+"\n");
                }
               
            }
        }
    }
    
    public void webBrowser(){
        WebView web = new WebView();
        WebEngine e = web.getEngine();
        e.load("https://www.google.com/");
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(web);
        Scene scene = new Scene(stackPane);
        Stage stage = new Stage();
        stage.setTitle("Code Check - Results");
        stage.setScene(scene);
        stage.show();
        
        
    }

    public void handleAboutRequest() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Stage about = new Stage();
        BorderPane aboutInfo = new BorderPane();

        VBox info = new VBox();
        info.setAlignment(Pos.CENTER);
        Text name = new Text();
        name.setFont(new Font(20));
        name.setText(props.getProperty(APP_TITLE));

        Text author = new Text(props.getProperty(APP_AUTHOR));
        Text year = new Text(props.getProperty(APP_YEAR));

        info.getChildren().addAll(name, author, year);

        aboutInfo.setCenter(info);
        about.setWidth(200);
        about.setHeight(200);

        Scene scene = new Scene(aboutInfo);
        about.setScene(scene);
        about.show();

    }

}
