/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc;

/**
 *
 * @author KelvinWongNYC
 */
import sc.data.CodeCheckData;
import djf.AppTemplate;
import sc.file.CodeCheckFiles;
import sc.workspace.CodeCheckWorkspace;

public class CodeCheckApp extends AppTemplate {

    @Override
    public void buildAppComponentsHook() {
        dataComponent = new CodeCheckData(this);
        workspaceComponent = new CodeCheckWorkspace(this);
        fileComponent = new CodeCheckFiles(this);
    }

    public static void main(String[] args) {
        launch(args);

    }

}
